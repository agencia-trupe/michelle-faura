<div class="conteudo-clipping">
	<div class="interna">
		<div class="clipping-wrapper">
			<a href="javascript:void(0)" onclick = "document.getElementById('light1').style.display='block';document.getElementById('fade').style.display='block'" class="clipping">
				<img src="<?php echo base_url('assets/img/clipping/case1/thumb.jpg') ?>" alt="">
			</a>
			<a href="javascript:void(0)" onclick = "document.getElementById('light2').style.display='block';document.getElementById('fade').style.display='block'" class="clipping">
				<img src="<?php echo base_url('assets/img/clipping/case2/thumb.jpg') ?>" alt="">
			</a>
			<a href="javascript:void(0)" onclick = "document.getElementById('light3').style.display='block';document.getElementById('fade').style.display='block'" class="clipping">
				<img src="<?php echo base_url('assets/img/clipping/case3/thumb.jpg') ?>" alt="">
			</a>
			<a href="javascript:void(0)" onclick = "document.getElementById('light4').style.display='block';document.getElementById('fade').style.display='block'" class="clipping">
				<img src="<?php echo base_url('assets/img/clipping/case4/thumb.jpg') ?>" alt="">
			</a>
			<a href="javascript:void(0)" onclick = "document.getElementById('light5').style.display='block';document.getElementById('fade').style.display='block'" class="clipping">
				<img src="<?php echo base_url('assets/img/clipping/case5/thumb.jpg') ?>" alt="">
			</a>
			<a href="javascript:void(0)" onclick = "document.getElementById('light6').style.display='block';document.getElementById('fade').style.display='block'" class="clipping">
				<img src="<?php echo base_url('assets/img/clipping/case6/thumb.jpg') ?>" alt="">
			</a>
			<a href="javascript:void(0)" onclick = "document.getElementById('light7').style.display='block';document.getElementById('fade').style.display='block'" class="clipping">
				<img src="<?php echo base_url('assets/img/clipping/case7/thumb.jpg') ?>" alt="">
			</a>
        	<div id="light1" class="white_content">
        		<a class="fechar-clipping" href = "javascript:void(0)" onclick = "document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
	        	<div class="clearfix"></div>
	        	<div class="clipping-slider-wrapper">
	        		<div class="clipping-slide">
	        			<div class="">
	        				<div class="image-left">
	        					<img src="<?php echo base_url('assets/img/clipping/case1/1.jpg') ?>" alt="">
	        				</div>
	        				<div class="image-right">
	        					<img src="<?php echo base_url('assets/img/clipping/case1/2.jpg') ?>" alt="">
	        				</div>
	        			</div>
	        		</div>
	        	</div>
			</div>
			<div id="light2" class="white_content">
        		<a class="fechar-clipping" href = "javascript:void(0)" onclick = "document.getElementById('light2').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
	        	<div class="clearfix"></div>
	        	<div class="clipping-slider-wrapper">
	        		<div class="clipping-slide">
	        			<div class="">
	        				<div class="image-left">
	        					<img src="<?php echo base_url('assets/img/clipping/case2/1.jpg') ?>" alt="">
	        				</div>
	        				<div class="image-right">
	        					<img src="<?php echo base_url('assets/img/clipping/case2/2.jpg') ?>" alt="">
	        				</div>
	        			</div>
	        		</div>
	        	</div>
			</div>
			<div id="light3" class="white_content">
        		<a class="fechar-clipping" href = "javascript:void(0)" onclick = "document.getElementById('light3').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
	        	<div class="clearfix"></div>
	        	<div class="clipping-slider-wrapper">
	        		<div class="clipping-slide">
	        			<div class="">
	        				<div class="image-left">
	        					<img src="<?php echo base_url('assets/img/clipping/case3/1.jpg') ?>" alt="">
	        				</div>
	        				<div class="image-right">
	        					<img src="<?php echo base_url('assets/img/clipping/case3/2.jpg') ?>" alt="">
	        				</div>
	        			</div>
	        		</div>
	        	</div>
			</div>
			<div id="light4" class="white_content">
        		<a class="fechar-clipping" href = "javascript:void(0)" onclick = "document.getElementById('light4').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
	        	<div class="clearfix"></div>
	        	<div class="clipping-slider-wrapper">
	        		<div class="clipping-slide">
	        				<div class="image-left">
	        					<img src="<?php echo base_url('assets/img/clipping/case4/1.jpg') ?>" alt="">
	        				</div>
	        				<div class="image-right">
	        					<img src="<?php echo base_url('assets/img/clipping/case4/2.jpg') ?>" alt="">
	        				</div>
	        		</div>
	        	</div>
			</div>
			<div id="light5" class="white_content">
        		<a class="fechar-clipping" href = "javascript:void(0)" onclick = "document.getElementById('light5').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
	        	<div class="clearfix"></div>
	        	<div class="clipping-slider-wrapper">
	        		<div class="clipping-slider">
	        			<div class="">
	        				<div class="image-left">
	        					<img src="<?php echo base_url('assets/img/clipping/case5/1.jpg') ?>" alt="">
	        				</div>
	        				<div class="image-right">
	        					<img src="<?php echo base_url('assets/img/clipping/case5/2.jpg') ?>" alt="">
	        				</div>
	        			</div>
	        			<div>
	        				<div class="image-left">
	        					<img src="<?php echo base_url('assets/img/clipping/case5/3.jpg') ?>" alt="">
	        				</div>
	        				<div class="image-right">
	        					<img src="<?php echo base_url('assets/img/clipping/case5/4.jpg') ?>" alt="">
	        				</div>
	        			</div>
	        		</div>
	        	</div>
			</div>
			<div id="light6" class="white_content">
        		<a class="fechar-clipping" href = "javascript:void(0)" onclick = "document.getElementById('light6').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
	        	<div class="clearfix"></div>
	        	<div class="clipping-slider-wrapper">
	        		<div class="clipping-slide">
	        			<div class="">
	        				<div class="image-left">
	        					<img src="<?php echo base_url('assets/img/clipping/case6/1.jpg') ?>" alt="">
	        				</div>
	        				<div class="image-right">
	        					<img src="<?php echo base_url('assets/img/clipping/case6/2.jpg') ?>" alt="">
	        				</div>
	        			</div>
	        		</div>
	        	</div>
			</div>
			<div id="light7" class="white_content">
        		<a class="fechar-clipping" href = "javascript:void(0)" onclick = "document.getElementById('light7').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
	        	<div class="clearfix"></div>
	        	<div class="clipping-slider-wrapper">
	        		<div class="clipping-slide">
	        			<div class="">
	        				<div class="image-left">
	        					<img src="<?php echo base_url('assets/img/clipping/case7/1.jpg') ?>" alt="">
	        				</div>
	        				<div class="image-right">
	        					<img src="<?php echo base_url('assets/img/clipping/case7/2.jpg') ?>" alt="">
	        				</div>
	        			</div>
	        		</div>
	        	</div>
			</div>
        	<div id="fade" class="black_overlay"></div>
		</div>
	</div> 
</div>
<div class="clearfix"></div>