<div class="span9">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span6">
            <ul class="nav nav-pills">
                <li class="active"><a href="<?php echo base_url(); ?>turmas/lista">Listar turmas</a></li>
                <li>
                    <a href="<?php echo base_url(); ?>turmas/cadastra">Cadastrar turma</a>
                </li>
            </ul>
        </div>
        
    </div>
     <?php
        
            $tmpl = array (
                                'table_open'          => '<table class="table table-striped table-bordered table-condensed">',

                                'heading_row_start'   => '<tr>',
                                'heading_row_end'     => '</tr>',
                                'heading_cell_start'  => '<th>',
                                'heading_cell_end'    => '</th>',

                                'row_start'           => '<tr>',
                                'row_end'             => '</tr>',
                                'cell_start'          => '<td>',
                                'cell_end'            => '</td>',


                                'table_close'         => '</table>'
                        );

            $this->table->set_template($tmpl); 
            $this->table->set_heading(array('Data', 'Titulo', 'Detalhes'));
            foreach ($result as $item)
            {  
            $link  = '<div class="row-fluid">'.anchor('turmas/detalhe/'.$item->id, '<i class="icon-info-sign"></i> Detalhes', 'class="btn btn-mini btn-info"').'</div>';
            $link .= '<div class="row-fluid">'.anchor('turmas/edita/'.$item->id, '<i class="icon-pencil"></i> Editar', 'class="btn btn-mini btn-warning"').'</div>';
            
            $meta = $item->item->titulo;
            
            $nome = array('data' => $meta, 'class' => 'span5',);
            
            $detalhes = array('data' => $link, 'class' => 'span2');
            $this->table->add_row(array(date('m/d/Y', strtotime($item->data)), $nome, $detalhes));
            }
            echo $this->table->generate();

            ?> 
                <?php echo $this->pagination->create_links(); ?>
        

</div><!--/span-->
         
          
      