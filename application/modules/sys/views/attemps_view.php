<div class="span9">
  <h1>Registros do tipo <?php echo $tipo; ?></h1>
<?php
        
            $tmpl = array (
                                'table_open'          => '<table class="table table-striped table-bordered table-condensed">',

                                'heading_row_start'   => '<tr>',
                                'heading_row_end'     => '</tr>',
                                'heading_cell_start'  => '<th>',
                                'heading_cell_end'    => '</th>',

                                'row_start'           => '<tr>',
                                'row_end'             => '</tr>',
                                'cell_start'          => '<td>',
                                'cell_end'            => '</td>',


                                'table_close'         => '</table>'
                        );

            $this->table->set_template($tmpl); 
            $this->table->set_heading(array('Login', 'Ip' ,'Data'));
            foreach ($result as $item)
            {  
            $login = $item['login'];
            $ip = $item['ip_address'];
            
            
            $this->table->add_row(array($login, $ip, date('d/m/Y - h:i:s A', strtotime($item['time']))));
            }
            echo $this->table->generate();

            ?> 
                <?php echo $this->pagination->create_links(); ?>
        

</div>