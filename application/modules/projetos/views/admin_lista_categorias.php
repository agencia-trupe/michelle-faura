<div class="row-fluid">
    <div class="span12">
            <legend>Categorias de Projeto 
                <?php echo anchor('painel/projetos/cadastrar', 'Novo projeto', 'class="btn btn-info btn-mini"'); ?> 
                <?php echo anchor('painel/projetos/categorias/cadastrar', 'Nova categoria', 'class="btn btn-info btn-mini"'); ?>  
                <a href="#" class="ordenar-categorias btn btn-mini btn-info">ordenar categorias</a>
                <a href="#" class="salvar-ordem-categorias hide btn btn-mini btn-warning">salvar ordem</a>
            </legend>
            <div class="alert alert-info hide categorias-mensagem">
                <span>Para ordenar, clique no nome da categoria e arraste até a posição desejada</span>
                <a class="close" data-dismiss="alert" href="#">&times;</a>
            </div>

     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span6">Título</th>
                <th>Criação</th>
                <th>Atualização</th>
                <th>Usuário</th>
                <th class="span5"><i class="icon-cog"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php if($categorias): ?>
            <?php foreach ($categorias as $categoria): ?>
                <tr id="categoria_<?php echo $categoria->id ?>">
                    <td><?=$categoria->titulo; ?></td>
                    <td><?=date('d/m/Y', $categoria->created); ?></td>
                    <td>
                        <?php if($categoria->updated): ?>
                            <?=date('d/m/Y', $categoria->updated); ?>
                        <?php endif; ?>
                    </td>
                    <td><?=$this->tank_auth->get_username($categoria->user_id); ?></td>
                    <td><?=anchor('painel/projetos/categorias/subcategorias/lista/' . $categoria->id, '<i class="icon-th-list icon-white"></i> subcategorias', 'class="btn btn-mini btn-info"'); ?>
                        <?=anchor('projetos/admin_categorias/editar/' . $categoria->id, '<i class="icon-pencil icon-white"></i> editar', 'class="btn btn-mini btn-warning"'); ?>
                        <?=anchor('projetos/admin_categorias/deleta_categoria/' . $categoria->id, '<i class="icon-remove-circle icon-white" ></i> remover', 'class="btn btn-mini btn-danger"'); ?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
    </div>
</div>