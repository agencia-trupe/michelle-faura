<div class="row-fluid">
    <div class="span9">
        <legend>Projetos 
                <?php echo anchor('painel/projetos/cadastrar/' . $subcategoria_id, 'Novo', 'class="btn btn-info btn-mini"'); ?>  
                <a href="#" class="ordenar-projetos btn btn-mini btn-info">ordenar projetos</a>
                <a href="#" class="salvar-ordem-projetos hide btn btn-mini btn-warning">salvar ordem</a>
        </legend>
        <div class="alert alert-info hide projetos-mensagem">
            <span>Para ordenar, clique no nome do projeto e arraste até a posição desejada</span>
            <a class="close" data-dismiss="alert" href="#">&times;</a>
        </div>
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span6">Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php if($projetos): ?>
            <?php foreach ($projetos as $projeto): ?>
                <tr id="projeto_<?php echo $projeto->id ?>">
                    <td><?=$projeto->titulo; ?></td>
                    <td><?=anchor('projetos/admin_projetos/editar/' . $projeto->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                        <?=anchor('projetos/admin_projetos/deleta_projeto/' . $projeto->id, 'Remover', 'class="btn btn-mini btn-danger"'); ?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
    </div>
</div>