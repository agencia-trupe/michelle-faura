<div class="conteudo-projetos">

    <?php foreach ($projetos as $projeto): ?>
        <a href="<?php echo site_url('projetos/detalhe/' . $categoria . '/' . $subcategoria . '/' . $projeto->id) ?>" class="projeto-link">
            <img src="<?php echo base_url('assets/img/projetos/capas/' . $projeto->capa) ?>" alt="">
            <span class="projeto-img-hover"></span>
        </a>
    <?php endforeach ?>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>