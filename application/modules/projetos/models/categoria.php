<?php
class Categoria extends Datamapper
{
    var $table = 'categorias';

    function get_all()
    {
        $categoria = new Categoria();
        $categoria->order_by( 'ordem', 'ASC' )->get();
        $arr = array();
        foreach( $categoria->all as $categoria )
        {
            $arr[] = $categoria;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return FALSE;
    }
    function get_conteudo( $id )
    {
        $categoria = new Categoria();
        $categoria->where( 'id', $id )->get();
        if( ! $categoria->exists() ) NULL;
        return $categoria;
    }

    function insert($dados)
    {
        $categorias = new Categoria();
        $categorias->get();

        $count = $categorias->result_count();

        $categoria = new Categoria();
        foreach ($dados as $key => $value)
        {
            $categoria->$key = $value;
        }
        $categoria->ordem = $count;
        $categoria->created = time();

        $insert = $categoria->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

    function change($dados)
    {
        $categoria = new Categoria();
        $categoria->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update_data['updated'] = time();

        $update = $categoria->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function apaga($id)
    {
        $categoria = new Categoria();
        $categoria->where('id', $id)->get();
        if($categoria->delete())
        {
            return TRUE;
        }
        return FALSE;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $categoria = new Categoria();
            $categoria->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($categoria->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}