<?php
class Projeto extends Datamapper
{
    var $table = 'projetos';

    function get_all($value = null, $criteria = null)
    {
        $projeto = new Projeto();
        if($value)
        {
            $projeto->where($criteria, $value);
        }
        $projeto->order_by( 'ordem', 'ASC' )->get();
        $arr = array();
        foreach( $projeto->all as $projeto )
        {
            $arr[] = $projeto;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return FALSE;
    }

    function get_others()
    {
        $projeto = new Projeto();
        $projeto->order_by( 'id', 'RANDOM' )->limit(7)->get();
        $arr = array();
        foreach( $projeto->all as $projeto )
        {
            $arr[$projeto->id]['id'] = $projeto->id;
            $arr[$projeto->id]['subcategoria'] = $this->_get_subcategoria_slug($projeto->subcategorias_projetos_id);
            $arr[$projeto->id]['capa'] = $projeto->capa;
            $arr[$projeto->id]['titulo'] = $projeto->titulo;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return FALSE;
    }

    private function _get_subcategoria_slug($subcategoria_id)
    {
        $subcategoria = new Categoria();
        $subcategoria->where('id', $subcategoria_id)->get();
        return $subcategoria->slug;
    }

    function get_conteudo( $id )
    {
        $projeto = new Projeto();
        $projeto->where( 'id', $id )->get();
        if( ! $projeto->exists() ) NULL;
        return $projeto;
    }

    function get_related( $ordem, $position, $subcategoria_slug )
    {
        $subcategoria = new Tipo();
        $subcategoria->where('slug',$subcategoria_slug)->get();

        $projeto = new Projeto();
        $projeto->where('subcategorias_projetos_id',$subcategoria->id);

        switch ( $position ) {
            case 'prev':
                $projeto->order_by('ordem', 'DESC');
                $projeto->where( 'ordem <', $ordem );
                break;
            case 'next':
                $projeto->order_by('ordem', 'ASC');
                $projeto->where( 'ordem >', $ordem );
                break;
        }

        $projeto->get(1);
        if( ! $projeto->exists() ) return FALSE;
        
        return $projeto;
    }

    function insert($dados)
    {
        $projetos = new Projeto();
        $projetos->where('subcategoria_id', $dados['subcategoria_id'])->get();
        $count = $projetos->result_count();

        $projeto = new Projeto();
        foreach ($dados as $key => $value)
        {
            $projeto->$key = $value;
        }
        $projeto->ordem = $count;
        $projeto->created = time();
        $insert = $projeto->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

        function change($dados)
    {
        $projeto = new Projeto();
        $projeto->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update_data['updated'] = time();
        $update = $projeto->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function apaga($id)
    {
        $projeto = new Projeto();
        $projeto->where('id', $id)->get();
        if($projeto->delete())
        {
            return $projeto;
        }
        return FALSE;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $categoria = new Projeto();
            $categoria->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($categoria->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}