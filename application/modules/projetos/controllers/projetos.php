<?php
class Projetos extends MX_Controller
{
    var $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('projetos/projeto');
        $this->load->model('projetos/foto');
        $this->load->model('projetos/categoria');
        $this->load->model('projetos/subcategoria');
        $this->data['pagina'] = 'projetos';
    }
    public function index()
    {
        $this->lista();
    }
    public function lista($categoria = null, $subcategoria = null)
    {
        //caso nenhum tipo seja passado como parâmetro, o primeiro tipo da lista
        //é usado
        $categorias = $this->categoria->get_all();

        if($categoria == NULL || $subcategoria == NULL) 
        {
            $categoria = $categorias[0]->slug;
            $subcategorias = $this->subcategoria->get_all($categorias[0]->id);
            $subcategoria_id = $subcategorias[0]->id;
            $subcategoria = $subcategorias[0]->slug;
            $this->data[$subcategorias[0]->slug] = TRUE;
        } else {
            $subcategoria_object = $this->subcategoria->get_conteudo($subcategoria, 'slug');
            $subcategoria = $subcategoria_object->slug;
            $subcategoria_id = $subcategoria_object->id;
            $this->data[$subcategoria_object->slug] = TRUE;
        }

        //seleciona os projetos basendo-se no tipo
        $this->data['projetos'] = $this->projeto->get_all($subcategoria_id, 'subcategoria_id');
        $this->data['subcategoria'] = $subcategoria;
        $this->data['categoria'] = $categoria;

        $this->data['conteudo'] = 'projetos/index';

        $seo = array(
            'titulo' => 'Projetos',
            'descricao' =>  '   Conceça os projetos realizados pela Livia Bortoncello
                            arquitetura de interiores' 
            );
        $this->load->library( 'seo', $seo );
        $this->load->view( 'layout/template', $this->data );
    }

    public function detalhe($categoria, $subcategoria, $id)
    {
        //seleciona todos os tipos de projetos no banco de dados

        $this->load->model( 'projetos/foto' );
        //busca os dados de uma página cujo slug foi passado como parâmetro
        $projeto = $this->projeto->get_conteudo( $id , 'id' );

        if( !is_null( $projeto ) )
        {
            $this->data['fotos'] = $this->foto->get_projeto($projeto->id);

            $this->data['count'] = count($this->data['fotos']);
            //Variável com os dados da página a ser enviada para a view
            $this->data['pagina'] = 'projetos';

            $this->data['projeto'] = $projeto;
            //Outros projetos
            $this->data['outros'] = $this->projeto->get_others();
            
            $this->data[$subcategoria] = TRUE;

            $this->data['subcategoria'] = $subcategoria;

            $this->data['categoria'] = $categoria;
            
            

            //Carrega a biblioteca de SEO e a inicializa.
            $seo = array(
                'title' => $projeto->titulo,
                'description' => $projeto->descricao,
                );
            $this->load->library( 'seo', $seo );
            //Define a view utilizada
            $this->data['conteudo'] = 'projetos/detalhe';
            //Carrega a view especificada como parâmetro e exibe a página
            $this->load->view( 'layout/template', $this->data );
        }
        else
        {
            show_404();
        }
    }

    private function _insert()
    {
        for ($i=0; $i < 9; $i++) { 
            $dados = array();
            $dados['titulo'] = 'Projeto ' . $i;
            $dados['data'] = 2010;
            $dados['texto'] = 'Descrição do projeto ' . $i;
            $dados['capa'] = 'projeto_' . $i . '.jpg';
            $insert = $this->projeto->insert($dados);
            if($insert)
            {
                echo 'Ok!';
            }
            else
            {
                echo 'Nooooo!';
            }
        }
    }

    public function insert_foto($projeto_id)
    {
        for ($i=0; $i < 30; $i++) { 
            $dados = array();
            $dados['titulo'] = 'Foto ' . $i;
            $dados['imagem'] = 'foto_exemplo.jpg';
            $dados['projeto_id'] = $projeto_id;
            $insert = $this->foto->insert($dados);
            if($insert)
            {
                echo 'Ok!';
            }
            else
            {
                echo 'Nooooo!';
            }
        }
    }
}