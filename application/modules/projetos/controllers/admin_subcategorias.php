<?php
class Admin_subcategorias extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'projetos';
        $this->load->model('projetos/categoria');
        $this->load->model('projetos/subcategoria');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista($categoria_id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['categoria_id'] = $categoria_id;
            $this->data['subcategorias'] = $this->subcategoria->get_all($categoria_id);
            $this->data['conteudo'] = 'projetos/admin_lista_subcategorias';
            $this->load->view('start/template', $this->data);
        }
    }

    public function editar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'editar';
            $this->data['subcategoria'] = $this->subcategoria->get_conteudo($id, 'id');
            $this->data['categorias'] = $this->categoria->get_all();
            $this->data['categoria_id'] = $this->data['subcategoria']->id;
            $this->data['conteudo'] = 'projetos/admin_edita_subcategorias';
            $this->load->view('start/template', $this->data);
        }
    }

    public function cadastrar($categoria_id = NULL)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['categorias'] = $this->categoria->get_all();
            $this->data['categoria_id'] = $categoria_id;
            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'projetos/admin_edita_subcategorias';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa_cadastro()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('subcategorias'))
            {
                $this->data['acao'] = 'cadastrar';
                $this->data['conteudo'] = 'projetos/admin_edita_subcategorias';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                //Obtém o slug
                $this->load->helper('blog');
                $post['slug'] = get_slug($post['titulo']);

                //Id do usuário
                $post['user_id'] = $this->tank_auth->get_user_id();

                if($this->subcategoria->insert($post))
                {
                    $this->session->set_flashdata('success', 'Registro adicionado com sucesso');
                    redirect('painel/projetos/categorias/subcategorias/lista/' . $this->input->post('categoria_id'));
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível adicionar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/projetos/categorias/subcategorias/lista/' . $this->input->post('categoria_id'));
                }
            }
        }
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('subcategorias'))
            {
                $this->data['acao'] = 'editar';
                $this->data['subcategoria'] = $this->subcategoria->get_conteudo($this->input->post('id'), 'id');
                $this->data['conteudo'] = 'projetos/admin_edita_subcategorias';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }

                //Obtém o slug
                $this->load->helper('blog');
                $post['slug'] = get_slug($post['titulo']);

                //Id do usuário
                $post['user_id'] = $this->tank_auth->get_user_id();

                if($this->subcategoria->change($post))
                {
                    $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                    redirect('painel/projetos/categorias/subcategorias/lista/' . $this->input->post('categoria_id'));
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/projetos/categorias/subcategorias/editar/' . $post['id']);
                }
            }
        }
    }

    public function deleta_subcategoria($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $subcategoria = $this->subcategoria->get_conteudo($id, 'id');
            $apaga = $this->subcategoria->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/projetos/categorias/subcategorias/lista/' . $subcategoria->categoria_id);
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/projetos/categorias/subcategorias/lista/' . $subcategoria->categoria_id);
            }
        }
    }

    /**
     * Reordena os subcategorias de produtos para a exibição
     * @return void status do processamento
     */
    public function sort_subcategorias()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('subcategoria');
            if ($itens)
            {
                $ordenar = $this->subcategoria->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }
}