<nav>
            <ul>
                <li>
                    <a class="parent <?php echo ($pagina == 'escritorio') ? 'active' : '' ?>" href="<?php echo site_url('escritorio') ?>">
                        <span class="texto">O escritório</span>
                        <span class="barra-menu"></span>
                    </a>
                </li>
                <div class="clearfix"></div>
                <li class="projetos-menu <?php echo ($pagina == 'projetos') ? 'projetos-active' : ''; ?>">
                    <a class="parent <?php echo ($pagina == 'projetos') ? 'active' : ''; ?>" href="<?php echo site_url('projetos') ?>">
                        <span class="texto">Projetos</span>
                        <span class="barra-menu"></span>
                    </a>
                    <div class="clearfix"></div>
                    <div class="barra-menu"></div>
                    <div class="dropdown-content">
                        <a class="dropdown-titulo">Arquitetura</a>
                        <ul class="dropdown">
                            <li><a <?php echo (isset($casas)) ? 'class="active"' : '' ?> href="<?php echo site_url('projetos/arquitetura/casas') ?>">&middot;  casas</a></li>
                            <li><a <?php echo (isset($condominios)) ? 'class="active"' : '' ?> href="<?php echo site_url('projetos/arquitetura/condominios') ?>">&middot;  condomínios</a></li>
                            <li><a <?php echo (isset($comerciais)) ? 'class="active"' : ''?> href="<?php echo site_url('projetos/arquitetura/comerciais') ?>">&middot;  comerciais</a></li>
                            <li><a <?php echo (isset($edificios)) ? 'class="active"' : ''?> href="<?php echo site_url('projetos/arquitetura/edificios') ?>">&middot;  edifícios</a></li>
                        </ul><!--.dropdown-->

                        <a class="dropdown-titulo">Interiores</a>
                        <ul class="dropdown">
                            <li><a  <?php echo (isset($residencial)) ? 'class="active"' : ''?> href="<?php echo site_url('projetos/interiores/residencial') ?>">&middot;  residencial</a></li>
                            <li><a  <?php echo (isset($comercial)) ? 'class="active"' : ''?> href="<?php echo site_url('projetos/interiores/comercial') ?>">&middot;  comercial</a></li>
                        </ul><!--.dropdown-->

                        <a class="dropdown-titulo">Mostras</a>
                        <ul class="dropdown dropdown-mostras">
                            <li><a  <?php echo (isset($mostras)) ? 'class="active"' : ''?> href="<?php echo site_url('projetos/mostras') ?>">&middot;  mostras</a></li>
                        </ul><!--.dropdown-->
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li>
                    <a class="parent <?php echo ($pagina == 'midia') ? 'active' : ''; ?>" href="<?php echo site_url('midia') ?>">
                        <span class="texto">Mídia</span>
                        <span class="barra-menu"></span>
                    </a>
                    <div class="clearfix"></div>
                    <div class="barra-menu"></div>
                </li>
                <li>
                    <a class="parent <?php echo ($pagina == 'contato') ? 'active' : ''; ?>" href="<?php echo site_url('contato') ?>">
                        <span class="texto">Contato</span>
                        <span class="barra-menu"></span>
                    </a>
                    <div class="clearfix"></div>
                    <div class="barra-menu"></div>
                </li>


            </ul>
        </nav>
        <a href="<?php echo $contato->facebook ?>" class="facebook-sidebar"></a>
        <iframe src="//www.facebook.com/plugins/like.php?href=<?php echo site_url() ?>&amp;send=false&amp;layout=standard&amp;width=65&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=35&amp;appId=355115464591773" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:65px; height:24px; position:absolute; bottom:19px" allowTransparency="true"></iframe>