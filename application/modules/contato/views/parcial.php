<div class="contato-home-wrapper">
    <div class="sidebar-telefone">
        <span><?php echo $contato->ddd ?> <?php echo $contato->telefone ?> | <?php echo $contato->ddd2 ?> <?php echo $contato->telefone2 ?></span>
    </div><!--.sidebar-telefone-->
    <div class="sidebar-endereco">
        <?php echo $contato->endereco ?> &middot; <?php echo $contato->cidade ?> <?php echo $contato->uf ?> - <?php echo $contato->cep ?> <br>
    </div>
    <div class="sidebar-copy">
        &copy; 2013 Livia Bortoncello Arquitetura &middot; Todos os direitos reservados <br>
        Criação de sites: <a target="_blank" href="http://trupe.net">Trupe Agência Criativa</a>
    </div><!--.sidebar-endereco-->
</div><!--.contato-home-wrapper-->