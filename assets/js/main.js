$(function() {
  $('.slides').bxSlider({
    controls:false,
    pager:false,
    auto: true,
    mode: 'fade',
    responsive: true,
    speed: 3500,
  });
  $('a.projeto-thumb:odd').addClass('projeto-thumb-bottom');
  $('.projetos-detalhe-ampliada').bxSlider({
    pagerCustom: '#bx-pager',
    mode: 'fade',
    prevSelector:'.projetos-slide-prev',
    nextSelector:'.projetos-slide-next'
  });

  $(".telefone_form").focus(function () {
    $(this).mask("(99) 9999-9999?9");
  });
  $(".telefone_form").focusout(function () {
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if (phone.length > 10) {
      element.mask("(99) 99999-999?9");
    } else {
      element.mask("(99) 9999-9999?9");
    }
  });
  //Image fill slideshow home
});

function runSlider(){
      //altera o resultado do array retornado em serve_slides
    function returnAsset(item){
      return '/previa/assets/img/slides/' + item;
    }
    //Retorna o link para o projeto pai do slide atual
    function getSlideLink(id){
      return location.protocol + '//' + location.hostname + '/previa/projetos/detalhe/' + id;
    }
    //Iinicia o full screen slideshow
      $.getJSON('/previa/home/serve_slides', function(data) {
         $.backstretch(data.map(returnAsset), {
          duration:3000,
          fade:1500
        });
      });

}


$(document).ready(function() {
    var animationOffset = $('.projetos-detalhe-miniaturas-container').height() - $('.projetos-detalhe-miniaturas-inner').height();
    
    if ( $('.projetos-detalhe-miniaturas-inner').height() > $('.projetos-detalhe-miniaturas-container').height() ) {
        
        $("#down").toggle();
        var mTop = 0;
            $('#down').click(function () {
                var mTop =  parseInt($('.projetos-detalhe-miniaturas-inner').css('margin-top') );
                if( mTop >= animationOffset + 85 ){
                  $('.projetos-detalhe-miniaturas-inner').animate({ "marginTop": "-=98px" }, 800, function() { $('.projetos-detalhe-miniaturas-inner').stop(); });
                }
                else {
                  $('#down').hide();
                }
                return false;
            });        
    }
}); 

$(function() {
  $('.projetos-lista-slider').bxSlider();
  $('.clipping-slider').bxSlider({
    pager:false
  });

});
  
//Verifica o suporte ao HTML Placeholder - home/contato
jQuery(function() {
  jQuery.support.placeholder = false;
  test = document.createElement('input');
  if('placeholder' in test) jQuery.support.placeholder = true;
});

//Hack para navegadores que não oferecem suporte ao HTML5 Placeholder - home/contato
$(function() {
  if(!$.support.placeholder) { 
    var active = document.activeElement;
    $(':text').focus(function () {
      if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
        $(this).val('').removeClass('hasPlaceholder');
      }
    }).blur(function () {
      if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
        $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
      }
    });
    $(':text').blur();
    $(active).focus();
    $('form').submit(function () {
      $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
    });
  }
});

//Prepara a requisição ajax do tipo POST para enviar os dados do formulário de
//contato - contato
$(function() {
 $('#contato-form').submit(function() {
    $('#message').html('Enviando...'); 
    var form_data = {
      nome : $('.nome_form').val(),
      email : $('.email_form').val(),
      telefone : $('.telefone_form').val(),
      mensagem : $('.mensagem_form').val(),
       ajax : '1'
    };
    $.ajax({
      url: location.protocol + "//" + location.host + "/previa/contato/ajax_check",
      type: 'POST',
      async : false,
      data: form_data,
      success: function(msg) {
        alert(msg);
        if(msg === 'Mensagem enviada com sucesso!'){
          $('.nome_form, .email_form, .telefone_form, .mensagem_form').val('');
        }
      }
    });
    return false;
  });
}); 

